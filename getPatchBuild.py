#!/usr/bin/python3
# -*- coding: utf-8 -*-

from INSAPatcher import *
from pkg_resources import parse_version


logger.setLevel(logging.INFO)



#################
# Main settings #
#################

class Settings:
	""" Settings of script """
	
	BASEDIR=os.getcwd() + os.sep
	""" App startup directory"""
	
	VERSION="latest"
	""" Version (git tag) to retrieve, or "latest" to retrieve latest
	tag """
	
	GITSRCURL="https://gitlab.com/bitfireAT/davx5-ose.git"
	""" URL of upstream git repo """
	
	UPSTREAMSRCDIR="davx5-ose"
	""" Name of upstream's source directory """
	
	WORKDIR="work"
	""" Work directory """
	
	BUILDDIR="build"
	""" APKs build directory """
	
	RELEASEDIR="release"
	""" APKs save directory """
	
	SOURCEDIR="src"
	""" Source directory """
	
	BUILDTEMPLATEDIR="build-template"
	""" Directory containing files/dir to append in build-directory
	(ie. Android Studio config dirs) """
	
	APKBUILDNUMBERCACHEFILE = 'latestbuild.txt'
	""" File used to store latest apk build number """
	
	BUILDNUMBER = Android.getNextBuildNumber(APKBUILDNUMBERCACHEFILE)
	""" APK build number """
	
	SOURCESEXTENSIONS = ('.xml', '.java', '.kt', '.gradle', 'aidl', 'ISettingsProviderFactory')
	""" Extensions to source files to patch """
	
	APKDEBUGSRC = Filesystem.unixPathToOS(BUILDDIR + '/app/build/outputs/apk/standard/debug/app-standard-debug.apk')
	""" Path to debug apk, once built """
	
	APKUNSIGNEDSRC = Filesystem.unixPathToOS(BUILDDIR + '/app/build/outputs/apk/standard/release/app-standard-release-unsigned.apk')
	""" Path to release apk, once built """
	
	


#################################
# Retrieve latest DAVx⁵ sources #
#################################
def getDavx5Sources():
	logger.warning("Retrieving latest DAVx⁵ sources")
	os.chdir(Settings.BASEDIR)
	
	if not os.path.lexists(Settings.UPSTREAMSRCDIR):
		# First download from git repo
		Command.run(["git", "clone", "--recursive", Settings.GITSRCURL, "--quiet"])
	else:
		# Update from git repo
		os.chdir(Settings.BASEDIR + Settings.UPSTREAMSRCDIR)
		Command.run(["git", "pull", "--recurse-submodules", "--quiet"])
		os.chdir(Settings.BASEDIR)

	# Copy sources in work directory
	Filesystem.rmtree(Settings.WORKDIR)
	Filesystem.copytree(Settings.UPSTREAMSRCDIR, Settings.WORKDIR)
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)

	# Find latest tag
	if Settings.VERSION == "latest":
		(ret, out, err) = Command.run(["git", "tag", "-l"], stdout=Command.TOVAR)
		versions = out.split("\n")
		while '' in versions:
			versions.remove('')
		tmp = [ "calsync" + v + "calsync" for v in versions ]
		versions = [ v[7:-7] for v in sorted(tmp, key=parse_version) ]

		Settings.VERSION=""
		while not Settings.VERSION:
			Settings.VERSION = versions.pop()

	logger.error("*** Using DAVx⁵ %s ***" % (Settings.VERSION))

	# Tag selection
	Command.run(["git", "checkout", "tags/%s" % (Settings.VERSION), "--quiet"])
	Command.run(["git", "submodule", "update", "--quiet"])

	# Remove DAVx⁵ git files
	for d in (".", "cert4android", "dav4android", "ical4android", "vcard4android"):
		for sub in ('.git', '.gitignore', '.gitlab-ci.yml', '.gitmodules'):
			Filesystem.rm(d + os.sep + sub)









##############################
# Convert DAVx⁵ into calsync #
##############################
def convertDavx5ToCalsync():
	logger.warning("Converting DAVx⁵ into calsync")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	
	logger.info(" - Updating build versionCode")
	s = SourceFile('app/build.gradle')
	s.replaceByRegex(
		pattern = r'^( +versionCode) .*$',
		replacement = r'\1 %s' % (Settings.BUILDNUMBER)
	)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Renaming app")
	for f in Filesystem.findFiles(".", suffixes=Settings.SOURCESEXTENSIONS, filesOnly=True):
		s = SourceFile(f)
		s.enqueueReplace(old = 'bitfire.at', new = 'insa-strasbourg.fr', failsWhenUnchanged=False)
		s.enqueueReplace(old = 'at.bitfire.davdroid', new = 'fr.insa_strasbourg.calsync', failsWhenUnchanged = False)
		
		s.enqueueReplace(old = 'DAVx5', new = 'calSync', failsWhenUnchanged = False)
		s.enqueueReplace(old = 'DAVx⁵', new = 'calSync', failsWhenUnchanged = False)
		
		s.enqueueReplace(old = 'DAVdroid', new = 'calSync', failsWhenUnchanged = False)
		s.enqueueReplace(old = 'DAVDroid', new = 'calSync', failsWhenUnchanged = False)
		s.enqueueReplace(old = 'davdroid', new = 'calsync', failsWhenUnchanged = False)
		s.enqueueReplace(old = '?pk_campaign=calsync-app', new = '?pk_campaign=davdroid-app', failsWhenUnchanged = False)
		s.enqueueReplace(old = 'calsync.insa-strasbourg.fr', new = 'davdroid.bitfire.at', failsWhenUnchanged = False)
		s.processReplaceQueue()

	logger.info(" - Renaming directories")
	Filesystem.rename('app/src/main/java/at/bitfire/davdroid', 'app/src/main/java/at/bitfire/calsync')
	Filesystem.rename('app/src/main/java/at/bitfire', 'app/src/main/java/at/insa_strasbourg')
	Filesystem.rename('app/src/main/java/at', 'app/src/main/java/fr')
	
	Filesystem.rename('app/src/main/resources/META-INF/services/at.bitfire.davdroid.settings.ISettingsProviderFactory', 'app/src/main/resources/META-INF/services/fr.insa_strasbourg.calsync.settings.ISettingsProviderFactory')

	logger.info(" - Updating icons")
	Filesystem.copydircontent(r'../icons/res/', r"app/src/main/res/")
	Filesystem.copyfile(r'../icons/res/mipmap/ic_launcher.png', r"app/src/main/ic_launcher-web.png")
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Adding fr.insa_strasbourg.** to proguard's rules")
	s = SourceFile('app/proguard-rules.txt')
	original = """
-keep class at.bitfire.** { *; }       # all DAVx⁵ code is required"""
	patched = """
-keep class at.bitfire.** { *; }       # all DAVx⁵ code is required
-keep class fr.insa_strasbourg.** { *; }       # all calSync code is required"""
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Removing references to DAVx⁵'s twitter account, website, forum, etc. in UI")
	# To create this file, open calsync version imported from DAVx⁵ in
	# Android Studio and update these attributes (maybe you'll need to
	# rebuild app first) :
	# - component '@string/navigation_drawer_news_updates' : visible=False
	# - component 'nav_website' : visible=False
	# - component 'nav_faq' : visible=False
	# - component 'nav_forums' : visible=False
	s = SourceFile('app/src/main/res/menu/activity_accounts_drawer.xml')
	original = """
    <item android:title="@string/navigation_drawer_news_updates">
        <menu>
            <item
                android:id="@+id/nav_twitter"
                android:icon="@drawable/twitter"
                android:title="\@davx5app"
                tools:ignore="HardcodedText"/>
        </menu>
    </item>

    <item android:title="@string/navigation_drawer_external_links">
        <menu>
            <item
                android:id="@+id/nav_website"
                android:icon="@drawable/ic_home_dark"
                android:title="@string/navigation_drawer_website"/>
            <item
                android:id="@+id/nav_manual"
                android:icon="@drawable/ic_info_dark"
                android:title="@string/navigation_drawer_manual"/>
            <item
                android:id="@+id/nav_faq"
                android:icon="@drawable/ic_help_dark"
                android:title="@string/navigation_drawer_faq"/>
            <item
                android:id="@+id/nav_forums"
                android:icon="@drawable/ic_forum_dark"
                android:title="@string/navigation_drawer_forums"/>
"""

	patched = """
     <item
        android:title="@string/navigation_drawer_news_updates"
        android:visible="false">
        <menu>
            <item
                android:id="@+id/nav_twitter"
                android:icon="@drawable/twitter"
                android:title="\@davx5app"
                tools:ignore="HardcodedText"/>
        </menu>
    </item>

    <item android:title="@string/navigation_drawer_external_links">
        <menu>
            <item
                android:id="@+id/nav_website"
                android:icon="@drawable/ic_home_dark"
                android:title="@string/navigation_drawer_website"
                android:visible="false"/>
            <item
                android:id="@+id/nav_manual"
                android:icon="@drawable/ic_info_dark"
                android:title="@string/navigation_drawer_manual"
                android:visible="false"/>
            <item
                android:id="@+id/nav_faq"
                android:icon="@drawable/ic_help_dark"
                android:title="@string/navigation_drawer_faq"
                android:visible="false"/>
            <item
                android:id="@+id/nav_forums"
                android:icon="@drawable/ic_forum_dark"
                android:title="@string/navigation_drawer_forums"
                android:visible="false"/>
"""
	
	s.replace(original, patched)
	

	#-------------------------------------------------------------------
	
	
	logger.info(""" - Appending calsync's "About" tab, and complete DAVx⁵'s "About" tab (app name, website, license)""")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/ui/AboutActivity.kt')
	original = """
    private inner class TabsAdapter(
            fm: FragmentManager
    ): FragmentPagerAdapter(fm) {

        override fun getCount() = 2

        override fun getPageTitle(position: Int): String =
                when (position) {
                    1 -> getString(R.string.about_libraries)
                    else -> getString(R.string.app_name)
                }

        override fun getItem(position: Int) =
                when (position) {
                    1 -> LibsBuilder()
                            .withAutoDetect(false)
                            .withFields(R.string::class.java.fields)
                            .withLicenseShown(true)
                            .supportFragment()
                    else -> DavdroidFragment()
                }!!
    }


    class DavdroidFragment: Fragment(), LoaderManager.LoaderCallbacks<Spanned> {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
                inflater.inflate(R.layout.about_calsync, container, false)!!

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            app_name.text = getString(R.string.app_name)
            app_version.text = getString(R.string.about_version, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)
            build_time.text = getString(R.string.about_build_date, SimpleDateFormat.getDateInstance().format(BuildConfig.buildTime))

            pixels.text = HtmlCompat.fromHtml(pixelsHtml, HtmlCompat.FROM_HTML_MODE_LEGACY)

            if (true /* open-source version */) {
                warranty.setText(R.string.about_license_info_no_warranty)
                LoaderManager.getInstance(this).initLoader(0, null, this)
            }
        }

        override fun onCreateLoader(id: Int, args: Bundle?) =
                HtmlAssetLoader(requireActivity(), "gplv3.html")

        override fun onLoadFinished(loader: Loader<Spanned>, license: Spanned?) {
            Logger.log.info("LOAD FINISHED")
            license_text.text = license
        }

        override fun onLoaderReset(loader: Loader<Spanned>) {
        }

    }
"""
	patched = """
    private inner class TabsAdapter(
            fm: FragmentManager
    ): FragmentPagerAdapter(fm) {

        override fun getCount() = 3

        override fun getPageTitle(position: Int): String =
                when (position) {
                    2 -> getString(R.string.about_libraries)
                    1 -> getString(R.string.app_name_orig)
                    else -> getString(R.string.app_name)
                }

        override fun getItem(position: Int) =
                when (position) {
                    2 -> LibsBuilder()
                            .withAutoDetect(false)
                            .withFields(R.string::class.java.fields)
                            .withLicenseShown(true)
                            .supportFragment()
                    1 -> DavdroidFragment()
                    else -> CalsyncFragment()
                }!!
    }


    class DavdroidFragment: Fragment(), LoaderManager.LoaderCallbacks<Spanned> {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
                inflater.inflate(R.layout.about_davdroid, container, false)!!

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            app_name.text = getString(R.string.app_name_orig)
            app_version.text = getString(R.string.about_version, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)
            build_time.text = getString(R.string.about_build_date, SimpleDateFormat.getDateInstance().format(BuildConfig.buildTime))

            pixels.text = HtmlCompat.fromHtml(pixelsHtml, HtmlCompat.FROM_HTML_MODE_LEGACY)

            if (true /* open-source version */) {
                warranty.setText(R.string.about_license_info_no_warranty)
                LoaderManager.getInstance(this).initLoader(0, null, this)
            }
        }

        override fun onCreateLoader(id: Int, args: Bundle?) =
                HtmlAssetLoader(requireActivity(), "gplv3.html")

        override fun onLoadFinished(loader: Loader<Spanned>, license: Spanned?) {
            Logger.log.info("LOAD FINISHED")
            license_text.text = license
        }

        override fun onLoaderReset(loader: Loader<Spanned>) {
        }

    }


    class CalsyncFragment: Fragment(), LoaderManager.LoaderCallbacks<Spanned> {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
                inflater.inflate(R.layout.about_calsync, container, false)!!

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            app_name.text = getString(R.string.app_name)
            app_version.text = getString(R.string.about_version, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)
            build_time.text = getString(R.string.about_build_date, SimpleDateFormat.getDateInstance().format(BuildConfig.buildTime))

            pixels.text = HtmlCompat.fromHtml(pixelsHtml, HtmlCompat.FROM_HTML_MODE_LEGACY)

            if (true /* open-source version */) {
                warranty.setText(R.string.about_license_info_no_warranty)
                LoaderManager.getInstance(this).initLoader(0, null, this)
            }
        }

        override fun onCreateLoader(id: Int, args: Bundle?) =
                HtmlAssetLoader(requireActivity(), "gplv3.html")

        override fun onLoadFinished(loader: Loader<Spanned>, license: Spanned?) {
            Logger.log.info("LOAD FINISHED")
            license_text.text = license
        }

        override fun onLoaderReset(loader: Loader<Spanned>) {
        }

    }
"""
	s.replace(original, patched)
	

	#-------------------------------------------------------------------
	
	
	logger.info(""" - Updating DAVx⁵'s "About" tab""")
	Filesystem.copyfile(r'app/src/main/res/layout/about_davdroid.xml', r'app/src/main/res/layout/about_calsync.xml')
	s = SourceFile(r'app/src/main/res/layout/about_davdroid.xml')
	
	original = 'android:src="@mipmap/ic_launcher"'
	patched = 'android:src="@mipmap/ic_launcher_orig"'
	s.enqueueReplace(original, patched)
	
	original = 'android:text="calSync"'
	patched = 'android:text="DAVdroid"'
	s.enqueueReplace(original, patched)
	
	original = 'android:text="@string/about_copyright"'
	patched = 'android:text="@string/about_copyright_orig"'
	s.enqueueReplace(original, patched)
	
	original = '''
        <TextView
            android:id="@+id/build_time"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginBottom="16dp"
            android:textAlignment="center"
            android:text="@string/about_build_date" />'''
	patched = '''
        <TextView
            android:id="@+id/build_time"
            android:visibility="gone"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginBottom="16dp"
            android:textAlignment="center" />

        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginBottom="16dp"
            android:textAlignment="center"
            android:autoLink="web"
            android:text="@string/homepage_url_orig" />"'''
	s.enqueueReplace(original, patched)
	
	s.processReplaceQueue()
	

	#-------------------------------------------------------------------
	
	
	logger.info(""" - Updating DAVx⁵'s "About" menu""")
	Filesystem.copyfile(r'app/src/main/res/menu/about_davdroid.xml', r'app/src/main/res/menu/about_calsync.xml')
	s = SourceFile(r'app/src/main/res/menu/about_davdroid.xml')
	
	original = 'android:onClick="showWebsite"'
	patched = 'android:onClick="showWebsiteOrig"'
	s.replace(original, patched)

	
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/App.kt')
	
	original = """
        fun homepageUrl(context: Context) =
                Uri.parse(context.getString(R.string.homepage_url)).buildUpon()
                        .appendQueryParameter("pk_campaign", BuildConfig.APPLICATION_ID)
                        .appendQueryParameter("pk_kwd", context::class.java.simpleName)
                        .appendQueryParameter("app-version", BuildConfig.VERSION_NAME)
                        .build()!!
"""
	
	patched = """
        fun homepageUrl(context: Context) =
                Uri.parse(context.getString(R.string.homepage_url)).buildUpon()
                        .build()!!

        fun homepageUrlOrig(context: Context) =
                Uri.parse(context.getString(R.string.homepage_url_orig)).buildUpon()
                        .appendQueryParameter("pk_campaign", BuildConfig.APPLICATION_ID)
                        .appendQueryParameter("pk_kwd", context::class.java.simpleName)
                        .appendQueryParameter("app-version", BuildConfig.VERSION_NAME)
                        .build()!!
"""
	s.replace(original, patched)
	
	
	
	#-------------------------------------------------------------------
	
		
	logger.info(" - Updating 'about' string refs")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR + "/app/src/main/res/")
	for d in Filesystem.findFiles(".", prefixes=("values"), dirsOnly=True):
		f = Filesystem.unixPathToOS("%s/strings.xml" % (d))
		if os.path.isfile(f):
			s = SourceFile(f)
			s.replaceByRegex(
				pattern = r'^(.*<string name="app_name">.*)$',
				replacement = '\\1\n    <string name="app_name_orig">DAVx⁵</string>',
				failsWhenUnchanged = False
			)
			s.replace(
				old = '    <string name="homepage_url" translatable="false">https://www.davx5.com/</string>',
				new = '    <string name="homepage_url" translatable="false">https://sourcesup.renater.fr/calsync/</string>\n    <string name="homepage_url_orig" translatable="false">https://www.davx5.com/</string>',
				failsWhenUnchanged = False
			)
			s.replace(
				old = '    <string name="about_copyright" translatable="false">© Ricki Hirner, Bernhard Stockmann (bitfire web engineering)</string>',
				new = '    <string name="about_copyright" translatable="false">Code original de DAVx⁵ adapté par Boris Lechner (INSA Strasbourg), maintenu par Rudi KUNTZ (INSA Strasbourg)</string>\n    <string name="about_copyright_orig" translatable="false">© Ricki Hirner, Bernhard Stockmann (bitfire web engineering)</string>',
				failsWhenUnchanged = False
			)
			
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	
	#-------------------------------------------------------------------
	
		
	# Change message suggesting to donate in english and french languages,
	# delete this message in other languages in order to display english version,
	# because I'm unable to translate it in other languages
	logger.info(" - Updating message suggesting to donate")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR + "/app/src/main/res/")
	for d in Filesystem.findFiles(".", prefixes=("values"), dirsOnly=True):
		f = Filesystem.unixPathToOS("%s/strings.xml" % (d))
		if os.path.isfile(f):
			s = SourceFile(f)
			if d.endswith("values"):
				s.enqueueReplaceByRegex(
					pattern = r'(<string name="startup_donate_message">).*(</string>)',
					replacement = r'\1This software is heavily based on DAVx⁵ app. If you find CalSync useful, please consider a donation to DAVx⁵ team, or buy DAVx⁵ app on Google Play.\2'
				)
			elif d.endswith("values-fr"):
				s.enqueueReplaceByRegex(
					pattern = r'(<string name="startup_donate_message">).*(</string>)',
					replacement = r'''\1Cette application n\'est qu\'une légère adaptation d\'une autre application : DAVx⁵. Si CalSync vous rend service, merci d\'envisager de faire un don à l\'équipe de DAVx⁵ ou d\'acheter leur application sur Google Play.\2'''
				)
			else:
				s.enqueueReplaceByRegex(
					pattern = r'^.*(<string name="startup_donate_message">).*(</string>).*$',
					replacement = r'',
					failsWhenUnchanged = False
				)
			s.processReplaceQueue()
			
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Restoring donate url")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/ui/StartupDialogFragment.kt')
	original = """App.homepageUrl"""
	patched = """App.homepageUrlOrig"""
	s.replace(original, patched)
	
	
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/ui/DefaultAccountsDrawerHandler.kt')
	original = """App.homepageUrl"""
	patched = """App.homepageUrlOrig"""
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Removing help button from account creation activity")
	s = SourceFile('app/src/main/res/menu/activity_login.xml')
	s.replaceByRegex(pattern = r'^(.*android:onClick="showHelp".*)$', replacement = r'        android:visible="false"\n\1')
	
	
	#-------------------------------------------------------------------




	#-------------------------------------------------------------------
	
	
	s = SourceFile('app/build.gradle')
	original = """2.2.3.1-ose"""
	patched = """2.3-ose"""
	s.replace(original, patched)


	#-------------------------------------------------------------------



###############################
# Apply INSA theme to calsync #
###############################
def updateTheme():
	logger.warning("Applying calsync's theme")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	logger.info(" - Adding theme colors")
	s = SourceFile('app/src/main/res/values/styles.xml')
	original = """
    <!-- colors -->

"""
	patched = """
    <!-- colors -->

    <color name="red700_insa">#b71c1c</color>
    <color name="light_red_insa700">#c62828</color>
    <color name="light_red_insa500">#e42618</color>
    <color name="grey_insa500">#9e9e9e</color>
    <color name="white">#ffffff</color>
    
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	#-------------------------------------------------------------------


	logger.info(" - Using theme colors")
	for f in Filesystem.findFiles(".", suffixes=Settings.SOURCESEXTENSIONS, filesOnly=True):
		s = SourceFile(f)
		s.enqueueReplace(old = '@color/primaryColor', new = '@color/light_red_insa700', failsWhenUnchanged = False)
		s.enqueueReplace(old = '@color/primaryLightColor', new = '@color/light_red_insa500', failsWhenUnchanged = False)
		s.enqueueReplace(old = '@color/primaryDarkColor', new = '@color/red700_insa', failsWhenUnchanged = False)
		s.enqueueReplace(old = '@color/secondaryLightColor', new = '@color/grey_insa500', failsWhenUnchanged = False)
		s.enqueueReplace(old = '@color/actionBarButton', new = '#FFFFFFFF', failsWhenUnchanged = False)
		s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Adding toolbar theme")
	s = SourceFile('app/src/main/res/values/styles.xml')
	original = """
        <item name="preferenceTheme">@style/PreferenceThemeOverlay.v14.Material</item>
    </style>
"""
	patched = """
        <item name="actionBarTheme">@style/AppTheme.ActionBarTheme</item>
        <item name="preferenceTheme">@style/PreferenceThemeOverlay.v14.Material</item>
    </style>
"""
	s.enqueueReplace(original, patched)

	original = """
    <style name="AppTheme.NoActionBar">
        <item name="windowActionBar">false</item>
        <item name="windowNoTitle">true</item>
    </style>
"""
	patched = """
    <style name="AppTheme.NoActionBar">
        <item name="windowActionBar">false</item>
        <item name="windowNoTitle">true</item>
        <item name="actionBarTheme">@style/AppTheme.ActionBarTheme</item>
    </style>

    <style name="AppTheme.ActionBarTheme" parent="@style/ThemeOverlay.AppCompat.ActionBar">
        <item name="colorControlNormal">@android:color/white</item>
        <item name="titleTextColor">@android:color/white</item>
        <item name="android:actionMenuTextColor">@android:color/white</item>
        <!--
                <item name="subtitleTextColor">@android:color/white</item>
                <item name="android:textColorPrimary">@android:color/white</item>
                <item name="android:textColorSecondary">@android:color/white</item>
                <item name="android:textColor">@android:color/white</item>
        -->
    </style>

    <!--
        <style name="AppTheme.Toolbar" parent="Theme.AppCompat.Light">
            <item name="toolbarStyle">@style/AppTheme.Toolbar</item>
        </style>
    -->
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Applying toolbar theme to every toolbar")
	original = 'android:theme="?attr/actionBarTheme"'
	patched = 'android:theme="@style/AppTheme.ActionBarTheme"'

	s = SourceFile('app/src/main/res/layout/accounts_content.xml')
	s.replace(original, patched)

	s = SourceFile('app/src/main/res/layout/activity_about.xml')
	s.replace(original, patched)
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Updating adaptative icon")
	s = SourceFile('app/src/main/res/mipmap-anydpi-v26/ic_launcher.xml')
	original = """
	<background android:drawable="@color/light_red_insa700" />
	<foreground android:drawable="@drawable/ic_launcher_foreground" />
"""
	patched = """
	<background android:drawable="@color/white" />
	<foreground android:drawable="@mipmap/ic_launcher_foreground" />
"""
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------




##################################
# Apply INSA behavior to calsync #
##################################
def updateAppBehavior():
	logger.warning("Changing app behavior")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	
	logger.info(" - Forcing account creation by URL, on INSA's server")
	
	# Set default server URL and restrict account creation by URL
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/ui/setup/DefaultLoginCredentialsFragment.kt')
	s.enqueueReplace(old = r'val url = it.getStringExtra(LoginActivity.EXTRA_URL)', new = r'val url = "https://agenda.unistra.fr"')
	
	# Ensure login textarea gets focus
	original = """
        v.login_type_urlpwd_details.visibility = if (v.login_type_urlpwd.isChecked) View.VISIBLE else View.GONE
"""
	patched = """
        v.login_type_urlpwd_details.visibility = if (v.login_type_urlpwd.isChecked) View.VISIBLE else View.GONE
        if (v.login_type_urlpwd.isChecked) v.urlpwd_user_name.requestFocus()
"""
	s.enqueueReplace(original, patched)
	
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	# Account creation layout
	# To create this file, open calsync version imported from DAVx⁵ in
	# Android Studio and update these attributes (maybe you'll need to
	# rebuild app first) :
	# - component 'login_type_email' : visibility=gone
	# - component 'login_type_email_details' : visibility=gone
	# - component 'urlpwd_base_url' : enabled=false
	# - component 'login_type_urlcert' : visibility=gone
	# - component 'login_type_urlcert_details' : visibility=gone
	s = SourceFile('app/src/main/res/layout/login_credentials_fragment.xml')

	original = """
                android:id="@+id/login_type_email"
"""
	patched = """
                android:id="@+id/login_type_email"
                android:visibility="gone"
"""
	s.enqueueReplace(original, patched)
	
	
	original = """
                android:id="@+id/login_type_email_details"
"""
	patched = """
                android:id="@+id/login_type_email_details"
                android:visibility="gone"
"""
	s.enqueueReplace(original, patched)
	
	
	original = """
                        android:id="@+id/urlpwd_base_url"
"""
	patched = """
                        android:id="@+id/urlpwd_base_url"
                        android:enabled="false"
"""
	s.enqueueReplace(original, patched)
	
	
	original = """
                android:id="@+id/login_type_urlcert"
"""
	patched = """
                android:id="@+id/login_type_urlcert"
                android:visibility="gone"
"""
	s.enqueueReplace(original, patched)
	
	
	original = """
                android:id="@+id/login_type_urlcert_details"
"""
	patched = """
                android:id="@+id/login_type_urlcert_details"
                android:visibility="gone"
"""
	s.enqueueReplace(original, patched)
	

	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Avoiding duplicated shared calendars as our caldav server is SOGo")
	# https://forums.bitfire.at/topic/1038/duplicated-shared-calendars
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/DavService.kt')
	original = """
            fun saveCollections() {
                db.delete(Collections._TABLE, "${HomeSets.SERVICE_ID}=?", arrayOf(service.toString()))
                for ((_,collection) in collections) {
                    val values = collection.toDB()
                    Logger.log.log(Level.FINE, "Saving collection", values)
                    values.put(Collections.SERVICE_ID, service)
                    db.insertWithOnConflict(Collections._TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE)
                }
            }
"""
	patched = """
            fun saveCollections() {
                db.delete(Collections._TABLE, "${HomeSets.SERVICE_ID}=?", arrayOf(service.toString()))
                for ((_,collection) in collections) {
                    val values = collection.toDB()
                    val principalUrl = readPrincipal().toString()
                    val baseurl = values.get("url").toString()
                    // INSA DEBUG
                    // Logger.log.log(Level.FINE, "INSA DEBUG : saving collection only if '$baseurl' starts with '$principalUrl'")
                    if (baseurl.startsWith(principalUrl, true)) {
                        Logger.log.log(Level.FINE, "Saving collection", values)
                        values.put(Collections.SERVICE_ID, service)
                        db.insertWithOnConflict(Collections._TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE)
                    }
                }
            }
"""
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Hiding carddav and webcal boxes")
	# Hide carddav and webcal boxes that are visible on load, because we're
	# sure they won't be used
	s = SourceFile('app/src/main/res/layout/activity_account.xml')

	# carddav
	original = """
        <androidx.cardview.widget.CardView
            android:id="@+id/carddav"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginBottom="8dp"
            app:cardUseCompatPadding="true"
            app:cardElevation="8dp">
"""
	patched = """
        <androidx.cardview.widget.CardView
            android:id="@+id/carddav"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginBottom="8dp"
            android:visibility="gone"
            app:cardUseCompatPadding="true"
            app:cardElevation="8dp">
"""
	s.enqueueReplace(original, patched)

	# webcal
	original = """
        <androidx.cardview.widget.CardView
            android:id="@+id/webcal"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:cardUseCompatPadding="true"
            app:cardElevation="8dp">
"""
	patched = """
        <androidx.cardview.widget.CardView
            android:id="@+id/webcal"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:visibility="gone"
            app:cardUseCompatPadding="true"
            app:cardElevation="8dp">
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Disabling debug details on error")
	# Disable debug activity when an error occurs (but let the notification)
	original = """
                .setContentIntent(PendingIntent.getActivity(context, 0, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT))
"""
	patched = """
                //.setContentIntent(PendingIntent.getActivity(context, 0, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT))
"""
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/syncadapter/SyncManager.kt')
	s.replace(original, patched)

	original = """
                        .setContentIntent(PendingIntent.getActivity(this, 0, debugIntent, PendingIntent.FLAG_UPDATE_CURRENT))
"""
	patched = """
                        //.setContentIntent(PendingIntent.getActivity(this, 0, debugIntent, PendingIntent.FLAG_UPDATE_CURRENT))
"""
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/DavService.kt')
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Disabling unused permissions")
	
	# Disable contact permissions requirements and contactsync service in main manifest
	s = SourceFile('app/src/main/AndroidManifest.xml')
	original = """
    <!-- android.permission-group.CONTACTS -->
    <uses-permission android:name="android.permission.READ_CONTACTS"/>
    <uses-permission android:name="android.permission.WRITE_CONTACTS"/>
"""
	patched = """
    <!-- android.permission-group.CONTACTS -->
    <!-- <uses-permission android:name="android.permission.READ_CONTACTS"/>
    <uses-permission android:name="android.permission.WRITE_CONTACTS"/> -->
"""
	s.enqueueReplace(original, patched)

	original = """
        <service
            android:name=".syncadapter.ContactsSyncAdapterService"
            android:exported="true"
            tools:ignore="ExportedService">
            <intent-filter>
                <action android:name="android.content.SyncAdapter"/>
            </intent-filter>

            <meta-data
                android:name="android.content.SyncAdapter"
                android:resource="@xml/sync_contacts"/>
            <meta-data
                android:name="android.provider.CONTACTS_STRUCTURE"
                android:resource="@xml/contacts"/>
        </service>
"""
	patched = """
        <!-- <service
            android:name=".syncadapter.ContactsSyncAdapterService"
            android:exported="true"
            tools:ignore="ExportedService">
            <intent-filter>
                <action android:name="android.content.SyncAdapter"/>
            </intent-filter>

            <meta-data
                android:name="android.content.SyncAdapter"
                android:resource="@xml/sync_contacts"/>
            <meta-data
                android:name="android.provider.CONTACTS_STRUCTURE"
                android:resource="@xml/contacts"/>
        </service> -->
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------


	# Disabling contact permissions checks in account activity code
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/ui/AccountActivity.kt')
	original = """
        if (info?.carddav != null) {
            // if there is a CardDAV service, ask for contacts permissions
            requiredPermissions += Manifest.permission.READ_CONTACTS
            requiredPermissions += Manifest.permission.WRITE_CONTACTS
        }
"""
	patched = """
        /*if (info?.carddav != null) {
            // if there is a CardDAV service, ask for contacts permissions
            requiredPermissions += Manifest.permission.READ_CONTACTS
            requiredPermissions += Manifest.permission.WRITE_CONTACTS
        }*/
"""
	s.enqueueReplace(original, patched)
	
	
	original = """
                                // update main account of address book accounts
                                if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED)
                                    try {
                                        requireActivity().contentResolver.acquireContentProviderClient(ContactsContract.AUTHORITY)?.let { provider ->
                                            for (addrBookAccount in accountManager.getAccountsByType(getString(R.string.account_type_address_book)))
                                                try {
                                                    val addressBook = LocalAddressBook(requireActivity(), addrBookAccount, provider)
                                                    if (oldAccount == addressBook.mainAccount)
                                                        addressBook.mainAccount = Account(newName, oldAccount.type)
                                                } finally {
                                                    @Suppress("DEPRECATION")
                                                    if (Build.VERSION.SDK_INT >= 24)
                                                        provider.close()
                                                    else
                                                        provider.release()
                                                }
                                        }
                                    } catch(e: Exception) {
                                        Logger.log.log(Level.SEVERE, "Couldn't update address book accounts", e)
                                    }
"""
	patched = """
                                /* // update main account of address book accounts
                                if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED)
                                    try {
                                        requireActivity().contentResolver.acquireContentProviderClient(ContactsContract.AUTHORITY)?.let { provider ->
                                            for (addrBookAccount in accountManager.getAccountsByType(getString(R.string.account_type_address_book)))
                                                try {
                                                    val addressBook = LocalAddressBook(requireActivity(), addrBookAccount, provider)
                                                    if (oldAccount == addressBook.mainAccount)
                                                        addressBook.mainAccount = Account(newName, oldAccount.type)
                                                } finally {
                                                    @Suppress("DEPRECATION")
                                                    if (Build.VERSION.SDK_INT >= 24)
                                                        provider.close()
                                                    else
                                                        provider.release()
                                                }
                                        }
                                    } catch(e: Exception) {
                                        Logger.log.log(Level.SEVERE, "Couldn't update address book accounts", e)
                                    } */
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------


	# Disabling contact permissions checks in debug activity code
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/ui/DebugInfoActivity.kt')
	original = """
            for (permission in arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS,
"""
	patched = """
            for (permission in arrayOf(
"""
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------


	# Disabling ContactsProviders calls
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/calsync/syncadapter/AddressBooksSyncAdapterService.kt')
	original = """
        override fun sync(account: Account, extras: Bundle, authority: String, provider: ContentProviderClient, syncResult: SyncResult) {
            try {
                val accountSettings = AccountSettings(context, account)

                /* don't run sync if
                   - sync conditions (e.g. "sync only in WiFi") are not met AND
                   - this is is an automatic sync (i.e. manual syncs are run regardless of sync conditions)
                 */
                if (!extras.containsKey(ContentResolver.SYNC_EXTRAS_MANUAL) && !checkSyncConditions(accountSettings))
                    return

                updateLocalAddressBooks(account, syncResult)

                for (addressBookAccount in LocalAddressBook.findAll(context, null, account).map { it.account }) {
                    Logger.log.log(Level.INFO, "Running sync for address book", addressBookAccount)
                    val syncExtras = Bundle(extras)
                    syncExtras.putBoolean(ContentResolver.SYNC_EXTRAS_IGNORE_SETTINGS, true)
                    syncExtras.putBoolean(ContentResolver.SYNC_EXTRAS_IGNORE_BACKOFF, true)
                    ContentResolver.requestSync(addressBookAccount, ContactsContract.AUTHORITY, syncExtras)
                }
            } catch (e: Exception) {
                Logger.log.log(Level.SEVERE, "Couldn't sync address books", e)
            }

            Logger.log.info("Address book sync complete")
        }
"""
	patched = """
        override fun sync(account: Account, extras: Bundle, authority: String, provider: ContentProviderClient, syncResult: SyncResult) {
                Logger.log.log(Level.INFO, "Won't sync address books")
                syncResult.databaseError = true
        }
"""
	s.enqueueReplace(original, patched)
	
	
	original = """
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    if (remote.isEmpty()) {
                        Logger.log.info("No contacts permission, but no address book selected for synchronization")
                        return
                    } else {
                        // no contacts permission, but address books should be synchronized -> show notification
                        val intent = Intent(context, AccountActivity::class.java)
                        intent.putExtra(AccountActivity.EXTRA_ACCOUNT, account)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                        notifyPermissions(intent)
                    }
                }
"""
	patched = """
                /* if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    if (remote.isEmpty()) {
                        Logger.log.info("No contacts permission, but no address book selected for synchronization")
                        return
                    } else {
                        // no contacts permission, but address books should be synchronized -> show notification
                        val intent = Intent(context, AccountActivity::class.java)
                        intent.putExtra(AccountActivity.EXTRA_ACCOUNT, account)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                        notifyPermissions(intent)
                    }
                }*/
                
                Logger.log.info("No contacts permission, but no address book selected for synchronization")
                return
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------



#####################
# Source dir update #
#####################
def saveCalsyncSources():
	logger.warning("Saving source dir")
	os.chdir(Settings.BASEDIR)
	Filesystem.rmtree(Settings.SOURCEDIR)
	Filesystem.rename(Settings.WORKDIR, Settings.SOURCEDIR)
	
	



##############
# Build APKs #
##############
def buildApks():
	logger.warning("Building apks")
	os.chdir(Settings.BASEDIR)
	
	# Creating build dir
	Filesystem.rmtree(Settings.BUILDDIR)
	Filesystem.copytree(Settings.SOURCEDIR, Settings.BUILDDIR)
	if os.path.isdir(Settings.BUILDTEMPLATEDIR):
		Filesystem.copydircontent(Settings.BUILDTEMPLATEDIR, Settings.BUILDDIR)
		
	# Paths
	apkdebug = Filesystem.unixPathToOS(Settings.RELEASEDIR + '/calsync-%s-debug.apk' % (Settings.VERSION))
	apkunsigned = Filesystem.unixPathToOS(Settings.RELEASEDIR + '/calsync-%s-release-unsigned.apk' % (Settings.VERSION))
	apksigned = Filesystem.unixPathToOS(Settings.RELEASEDIR + '/calsync-%s-release-signed.apk' % (Settings.VERSION))
	
	
	#~ # Build debug apk
	#~ logger.info(" - Building debug APK")
	#~ os.chdir(Settings.BASEDIR + Settings.BUILDDIR)
	#~ Command.run(["gradlew.bat", "assembleDebug"])
	#~ os.chdir(Settings.BASEDIR)
	#~ Filesystem.rmfile(apkdebug)
	#~ Filesystem.rename(Settings.APKDEBUGSRC, apkdebug)
	
	
	# Build unsigned release apk
	logger.info(" - Building release APK")
	os.chdir(Settings.BASEDIR + Settings.BUILDDIR)
	Command.run(["gradlew.bat", "assembleRelease"])
	os.chdir(Settings.BASEDIR)
	Filesystem.rmfile(apkunsigned)
	Filesystem.rename(Settings.APKUNSIGNEDSRC, apkunsigned)

	# Sign release APK
	logger.info(" - Signing release APK")
	Android.signApk(apkunsigned, apksigned, "keystore.jks", "key0")

	# Delete unsigned APK
	logger.info(" - Deleting unsigned APK")
	Filesystem.rmfile(apkunsigned)

	# Save current build number as latest
	Android.commitCurrentBuild(Settings.APKBUILDNUMBERCACHEFILE)




def clean():
	# Stop all running gradle daemons
	if os.path.isdir(Settings.BASEDIR + Settings.BUILDDIR):
		os.chdir(Settings.BASEDIR + Settings.BUILDDIR)
		Command.run(["gradlew.bat", "--stop"])
	elif os.path.isdir(Settings.BASEDIR + Settings.SOURCEDIR):
		os.chdir(Settings.BASEDIR + Settings.SOURCEDIR)
		Command.run(["gradlew.bat", "--stop"])
	
	
	os.chdir(Settings.BASEDIR)
	Filesystem.mkdir(Settings.RELEASEDIR)
	Filesystem.rmtree(Settings.BUILDDIR)
	Filesystem.rmtree(Settings.WORKDIR)
	Filesystem.rmtree("__pycache__")




if __name__ == '__main__':
	try:
		clean()
		getDavx5Sources()
		convertDavx5ToCalsync()
		updateTheme()
		updateAppBehavior()
		saveCalsyncSources()
		buildApks()
	except SystemExit:
		pass
	except KeyboardInterrupt:
		logger.error("Keyboard interrupt")
	finally:
		clean()
