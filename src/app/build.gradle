/*
 * Copyright (c) Ricki Hirner (bitfire web engineering).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-android-extensions'
apply plugin: 'org.jetbrains.dokka-android'

android {
    compileSdkVersion 28
    buildToolsVersion '28.0.3'

    defaultConfig {
        applicationId "fr.insa_strasbourg.calsync"

        versionCode 216
        buildConfigField "long", "buildTime", System.currentTimeMillis() + "L"
        buildConfigField "boolean", "customCerts", "true"

        minSdkVersion 19        // Android 4.4
        targetSdkVersion 28     // Android 9.0

        buildConfigField "String", "userAgent", "\"calSync\""

        // when using this, make sure that notification icons are real bitmaps
        vectorDrawables.useSupportLibrary = true
    }

    flavorDimensions "distribution"
    productFlavors {
        standard {
            versionName "2.3-ose"
        }
    }

    buildTypes {
        debug {
            minifyEnabled false
        }
        release {
            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.txt'
        }
    }

    lintOptions {
        disable 'GoogleAppIndexingWarning'      // we don't need Google indexing, thanks
        disable 'ImpliedQuantity', 'MissingQuantity'		// quantities from Transifex may vary
        disable 'MissingTranslation', 'ExtraTranslation'	// translations from Transifex are not always up to date
        disable "OnClick"     // doesn't recognize Kotlin onClick methods
        disable 'RtlEnabled'
        disable 'RtlHardcoded'
        disable 'Typos'
    }
    packagingOptions {
        exclude 'META-INF/DEPENDENCIES'
        exclude 'META-INF/LICENSE'
    }

    defaultConfig {
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }
}

dependencies {
    implementation project(':cert4android')
    implementation project(':ical4android')
    implementation project(':vcard4android')

    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"

    implementation 'androidx.appcompat:appcompat:1.0.2'
    implementation 'androidx.cardview:cardview:1.0.0'
    implementation 'androidx.fragment:fragment:1.0.0'
    implementation 'androidx.preference:preference:1.0.0'
    implementation 'com.google.android.material:material:1.0.0'

    implementation(':dav4jvm') {
        exclude group: 'org.ogce', module: 'xpp3'	// Android comes with its own XmlPullParser
    }
    implementation 'com.jaredrummler:colorpicker:1.0.5'
    implementation 'com.mikepenz:aboutlibraries:6.2.0'

    implementation 'com.squareup.okhttp3:logging-interceptor:3.12.1'
    implementation 'commons-io:commons-io:2.6'
    implementation 'dnsjava:dnsjava:2.1.8'
    implementation 'org.apache.commons:commons-lang3:3.8.1'
    implementation 'org.apache.commons:commons-collections4:4.2'

    // for tests
    androidTestImplementation 'androidx.test:runner:1.1.1'
    androidTestImplementation 'androidx.test:rules:1.1.1'
    androidTestImplementation 'junit:junit:4.12'
    androidTestImplementation 'com.squareup.okhttp3:mockwebserver:3.12.1'

    testImplementation 'junit:junit:4.12'
    testImplementation 'com.squareup.okhttp3:mockwebserver:3.12.1'
}
