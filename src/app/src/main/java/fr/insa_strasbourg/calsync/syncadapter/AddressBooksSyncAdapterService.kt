/*
 * Copyright © Ricki Hirner (bitfire web engineering).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package fr.insa_strasbourg.calsync.syncadapter

import android.Manifest
import android.accounts.Account
import android.content.*
import android.content.pm.PackageManager
import android.database.DatabaseUtils
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import androidx.core.content.ContextCompat
import fr.insa_strasbourg.calsync.log.Logger
import fr.insa_strasbourg.calsync.model.CollectionInfo
import fr.insa_strasbourg.calsync.model.ServiceDB
import fr.insa_strasbourg.calsync.model.ServiceDB.Collections
import fr.insa_strasbourg.calsync.resource.LocalAddressBook
import fr.insa_strasbourg.calsync.settings.AccountSettings
import fr.insa_strasbourg.calsync.ui.AccountActivity
import okhttp3.HttpUrl
import java.util.logging.Level

class AddressBooksSyncAdapterService : SyncAdapterService() {

    override fun syncAdapter() = AddressBooksSyncAdapter(this)


    class AddressBooksSyncAdapter(
            context: Context
    ) : SyncAdapter(context) {

        override fun sync(account: Account, extras: Bundle, authority: String, provider: ContentProviderClient, syncResult: SyncResult) {
                Logger.log.log(Level.INFO, "Won't sync address books")
                syncResult.databaseError = true
        }

        private fun updateLocalAddressBooks(account: Account, syncResult: SyncResult) {
            ServiceDB.OpenHelper(context).use { dbHelper ->
                val db = dbHelper.readableDatabase

                fun getService() =
                        db.query(ServiceDB.Services._TABLE, arrayOf(ServiceDB.Services.ID),
                                "${ServiceDB.Services.ACCOUNT_NAME}=? AND ${ServiceDB.Services.SERVICE}=?",
                                arrayOf(account.name, ServiceDB.Services.SERVICE_CARDDAV), null, null, null)?.use { c ->
                            if (c.moveToNext())
                                c.getLong(0)
                            else
                                null
                        }

                fun remoteAddressBooks(service: Long?): MutableMap<HttpUrl, CollectionInfo> {
                    val collections = mutableMapOf<HttpUrl, CollectionInfo>()
                    service?.let {
                        db.query(Collections._TABLE, null,
                                Collections.SERVICE_ID + "=? AND " + Collections.SYNC, arrayOf(service.toString()), null, null, null)?.use { cursor ->
                            while (cursor.moveToNext()) {
                                val values = ContentValues(cursor.columnCount)
                                DatabaseUtils.cursorRowToContentValues(cursor, values)
                                val info = CollectionInfo(values)
                                collections[info.url] = info
                            }
                        }
                    }
                    return collections
                }

                // enumerate remote and local address books
                val service = getService()
                val remote = remoteAddressBooks(service)

                /* if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    if (remote.isEmpty()) {
                        Logger.log.info("No contacts permission, but no address book selected for synchronization")
                        return
                    } else {
                        // no contacts permission, but address books should be synchronized -> show notification
                        val intent = Intent(context, AccountActivity::class.java)
                        intent.putExtra(AccountActivity.EXTRA_ACCOUNT, account)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                        notifyPermissions(intent)
                    }
                }*/
                
                Logger.log.info("No contacts permission, but no address book selected for synchronization")
                return

                val contactsProvider = context.contentResolver.acquireContentProviderClient(ContactsContract.AUTHORITY)
                try {
                    if (contactsProvider == null) {
                        Logger.log.severe("Couldn't access contacts provider")
                        syncResult.databaseError = true
                        return
                    }

                    // delete/update local address books
                    for (addressBook in LocalAddressBook.findAll(context, contactsProvider, account)) {
                        val url = HttpUrl.parse(addressBook.url)!!
                        val info = remote[url]
                        if (info == null) {
                            Logger.log.log(Level.INFO, "Deleting obsolete local address book", url)
                            addressBook.delete()
                        } else {
                            // remote CollectionInfo found for this local collection, update data
                            try {
                                Logger.log.log(Level.FINE, "Updating local address book $url", info)
                                addressBook.update(info)
                            } catch (e: Exception) {
                                Logger.log.log(Level.WARNING, "Couldn't rename address book account", e)
                            }
                            // we already have a local address book for this remote collection, don't take into consideration anymore
                            remote -= url
                        }
                    }

                    // create new local address books
                    for ((_, info) in remote) {
                        Logger.log.log(Level.INFO, "Adding local address book", info)
                        LocalAddressBook.create(context, contactsProvider, account, info)
                    }
                } finally {
                    @Suppress("DEPRECATION")
                    if (Build.VERSION.SDK_INT >= 24)
                        contactsProvider?.close()
                    else
                        contactsProvider?.release()
                }
            }
        }

    }

}
